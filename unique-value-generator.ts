export type UniqueNumberGenerator = {
    generate: () => Promise<number>
}

/**
 * This is a system-wide unique value generator
 * Meaning if any more processes/services would use this generator 
 * it should guarantee uniqueness among all of those.
 * This process would have to ensure that
 * counter does not repeat among instances using some extra service .
 */
export  function createUniqueNumberGenerator(): UniqueNumberGenerator {
    // Currently we are being pretty naive thinking that
    // there will be only one instance of this app.
    // This should be persisted as well
    let newValue = 123456789;
    
    return {
        generate: () => Promise.resolve(newValue++)
    }
}