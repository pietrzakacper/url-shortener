import { createServer, IncomingMessage, ServerResponse } from 'http'
import * as url from 'url'
import { createStorage, Storage } from './storage'
import { encodeShortUrlEnd } from './encode-short-url-end'
import { createUniqueNumberGenerator, UniqueNumberGenerator } from './unique-value-generator'

const SHORT_URL_BASE = 'tier.app'

export async function startApp() {
    const server = createServer()
    const uniqueValueGenerator = createUniqueNumberGenerator()
    const storage = createStorage()

    // @TODO handle request, response errors in some kind of middleware
    server.on('request', createShortenUrlHandler(uniqueValueGenerator, storage))

    // @TODO actually allow for unshortening of the url
    // It was a bit misleading in the excercise description whether it should be implemented
    // So I focused on other parts ;) But it should be fairly simple now

    const port = 8080
    server.listen(port, () => {
        console.log(`Server is listening on 0.0.0.0:${port}`)
    })
}

function createShortenUrlHandler(
    uniqueNumberGenerator: UniqueNumberGenerator,
    storage: Storage,
) {
   return async (req: IncomingMessage, res: ServerResponse) => {
        const { url: longUrl } = url.parse(req.url!, true).query
    
        if (typeof longUrl !== 'string') {
            res.statusCode = 400
            return res.end()
        }
    
        const uniqueNumber = await uniqueNumberGenerator.generate()

        const shortUrlEnd = encodeShortUrlEnd(uniqueNumber)

        await storage.set(shortUrlEnd, longUrl)
        
        const shortUrl = `${SHORT_URL_BASE}/${shortUrlEnd}`
    
        res.end(shortUrl)
    }
}
