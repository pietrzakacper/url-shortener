// @TODO we need persistance
// My first choice would probably be Redis since we probably care about read speed
export type Storage = {
    set: (key: string, value: string) => Promise<void>
    get: (key: string) => Promise<string>
}

export function createStorage(): Storage {
   const map = new Map<string, string>()
    
   return {
    set: (key, value) => {
        map.set(key, value)
    
        return Promise.resolve()
    },
        
    get: (key) => {
        const value = map.get(key)
    
        if (!value) {
            return Promise.reject(`No entry for key ${key}`)
        }
    
        return Promise.resolve(value)
    }
   }
}
