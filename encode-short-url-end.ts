import createBase from 'base-x'

const BASE58 = '123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz'
const base58 = createBase(BASE58)

// @TODO change in case of using BigNumber for example
const SEED_BYTE_SIZE = 4

export function encodeShortUrlEnd(seed: number) {
    const buff = new ArrayBuffer(SEED_BYTE_SIZE)
    const dataView = new DataView(buff)
    
    dataView.setInt32(0, seed)

    const bytesArray = new Uint8Array(SEED_BYTE_SIZE);
    for(let offset = 0; offset < SEED_BYTE_SIZE; offset++) {
        bytesArray.set([dataView.getUint8(offset)], offset)
    }
    
   return base58.encode(bytesArray)
}